import numpy as np
from matplotlib import pyplot as plt


class BayesianCovid:
    def __init__(self, *, p_covid, p_loss, p_loss_g_covid):
        self.p_covid = p_covid
        self.p_loss = p_loss
        self.p_loss_g_covid = p_loss_g_covid

    def calc_p_covid_g_loss(self, *, p_covid=None, p_loss=None, p_loss_g_covid=None):
        if p_covid is None:
            p_covid = self.p_covid

        if p_loss is None:
            p_loss = self.p_loss

        if p_loss_g_covid is None:
            p_loss_g_covid = self.p_loss_g_covid

        return p_loss_g_covid * p_covid / p_loss

    def __repr__(self):
        return f"BayesianCovid(p_covid={self.p_covid}, p_smell_loss={self.p_loss}, p_loss_g_covid={self.p_loss_g_covid})"


def main():
    covid = BayesianCovid(p_covid=0.2, p_loss=0.001, p_loss_g_covid=0.01)
    x = np.linspace(0, 1, 500)
    plt.plot(x, covid.calc_p_covid_g_loss(p_covid=x))
    plt.xlabel("p(C)")
    plt.ylabel("p(C|L)")
    plt.hlines(y=1, xmin=0, xmax=1, colors="red")
    plt.show()

    x = np.logspace(-3, 0, 500)
    plt.plot(x, covid.calc_p_covid_g_loss(p_loss=x))
    plt.xscale("log")
    plt.xlabel("p(L)")
    plt.ylabel("p(C|L)")
    plt.hlines(y=1, xmin=0.001, xmax=1, colors="red")
    plt.show()

    x = np.linspace(0, 0.05, 500)
    plt.plot(x, covid.calc_p_covid_g_loss(p_loss_g_covid=x))
    plt.xlabel("p(L|C)")
    plt.ylabel("p(C|L)")
    plt.hlines(y=1, xmin=0, xmax=0.05, colors="red")
    plt.show()


if __name__ == '__main__':
    main()

# Answer to the questions:
# Q: What happened?
# A: The calculated result indicates that the conditional probability
#    of having COVID given the loss of smell is 2. This is a wrong answer,
#    because probability has to be in range <0;1>
#
# Q: Why?
# A: The reason for the wrong answer is not a mistake in calculation or
#    wrong formula, but wrong assumptions. The numbers used for calculation
#    p(C) = 0.2, p(L) = 0.001 and p(L|C) = 0.01 do not represent a valid
#    state of the world. Assuming p(C) and p(L|C) we can calculate the lower
#    bound on p(L) (when all cases of loss of smell come from covid and nothing else).
#    Assume p(C|L) = 1 (L is a subset of C) and calculate p(L) in such case.
#    Using Bayes theorem: p(C|L) = p(L|C) * p(C) / p(L)
#    Substituting: 1 = 0.01 * 0.2 / p(L)
#    p(L) = 0.01 * 0.2 = 0.002
#    In the above case, which is the lower bound for the cases of the loss of smell
#    (the loss of smell is caused only by COVID and nothing else) we get the result
#    p(L) > 0.002 when there are other causes of the loss of smell (p(C|L) < 1).
#    This is violated by the initial assumption p(L) = 0.001, so we can conclude
#    that the assumptions were necessarily self-contradictory and thus impossible.
#
# Q: How to fig it?
# A: It is impossible to fix the problem while keeping the currently used data.
#    The data needs to be checked for correctness and actual data should be used
#    for the calculation.
